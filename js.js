
// bai 1
var layDiemVung = function (area) {
    if (area == "vungA") {
        return 2;
    }
    if (area == "vungB") {
        return 1;
    }
    if (area == "vungC") {
        return 0.5;
    }
    return 0;
}

var layDiemUuTien = function (sub) {
    switch (sub) {
        case 1: {
            return 2.5;
        }
        case 2: {
            return 1.5;
        }
        case 3: {
            return 1;
        }
        default: {
            return 0;
        }
    }
}

document.getElementById('btnKqBai1').addEventListener("click", function () {
    var arr = document.getElementsByTagName("input");
    var diemMon1 = arr[0].value * 1;
    var diemMon2 = arr[1].value * 1;
    var diemMon3 = arr[2].value * 1;
    var doiTuong = document.getElementById('subject').value * 1;
    var diemVung = document.getElementById('area').value;
    const diemChuan = 20;
    var kq = "";
    // var tongDiem = 0;
    var tongDiem = diemMon1 + diemMon2 + diemMon3 + layDiemVung(diemVung) + layDiemUuTien(doiTuong);
    if (tongDiem >= diemChuan && diemMon1 > 0 && diemMon2 > 0 && diemMon3 > 0) {
        kq = `Bạn Đã Đậu. Tổng điểm: ${tongDiem}`;
    }
    else {
        kq = `Bạn Đã Rớt. Tổng điểm: ${tongDiem}`;
    }
    // console.log(tongDiem);
    document.querySelector(".kqBai1").innerText = kq;
});


// <================================================================================================================================>
// Bai2

document.getElementById("btnKqBai2").addEventListener("click", function () {
    var soKwValue = document.getElementById("kw").value * 1;
    var soTienPhaiTra;
    const tien50KwDau = 500;
    const tien50KwKe = 650;
    const tien100KmKe = 850;
    const tien150KwKe = 1100;
    const tienKwConlai = 1300;


    if (soKwValue <= 50) {
        soTienPhaiTra = soKwValue * tien50KwDau;
        // console.log(50);
    }
    else if (soKwValue > 50 && soKwValue <= 100) {
        soTienPhaiTra = 50 * tien50KwDau + (soKwValue - 50) * tien50KwKe;
        // console.log("50 100");
    }
    else if (soKwValue > 100 && soKwValue <= 200) {
        soTienPhaiTra = (50 * tien50KwDau) + (50 * tien50KwKe) + (soKwValue - 50 - 50) * tien100KmKe;
        // console.log("100 200");
    }
    else if (soKwValue > 200 && soKwValue <= 350) {
        soTienPhaiTra = (50 * tien50KwDau) + (50 * tien50KwKe) + (100 * tien100KmKe) + (soKwValue - 50 - 50 - 200) * tien150KwKe;
        // console.log("200 350");
    }
    else {
        soTienPhaiTra = (50 * tien50KwDau) + (50 * tien50KwKe) + (100 * tien100KmKe) + (150 * tien150KwKe) + (soKwValue - 50 - 50 - 200 - 350) * tienKwConlai;
        // console.log("con lai");
    }
    var name = document.getElementById("name").value;
    var kq = soTienPhaiTra;
    document.querySelector(".nameUser").innerText = `Họ và tiên: ${name} ;`;
    document.querySelector(".kqBai2").innerText = `Tiền điện: ${kq.toLocaleString()}`;

})












const BT_1 = document.querySelector('.BT_1');
const BT_1_Active = document.getElementById('BT-1');
const BT_2 = document.querySelector('.BT_2');
const BT_2_Active = document.getElementById('BT-2');

BT_1.addEventListener('click', () => {
    BT_1_Active.classList.toggle('active')
    BT_2_Active.classList.remove('active')

})

BT_2.addEventListener('click', () => {
    BT_2_Active.classList.toggle('active')
    BT_1_Active.classList.remove('active')

})